﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Refit;

namespace UniversalMapProject.Services
{
    public interface IGoogleMapsApi
    {
        [Get("/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin={location}&destination={destination}&key={googleKey}")]
        Task<HttpResponseMessage> GetDirections(string location, string destination, string googleKey);


    }
}
