﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.CrossPlacePicker;
using Plugin.CrossPlacePicker.Abstractions;
using UniversalMapProject.ViewModels;
using UniversalMapProject.Views;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace UniversalMapProject
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            this.BindingContext = new MainPageViewModel();
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(18.4691796, -69.9403312), Distance.FromMiles(3.0)));
           
          
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                var result = await CrossPlacePicker.Current.Display(new CoordinateBounds(new Coordinates(18.4691796, -69.9403312),new Coordinates(18.4691796, -69.9403312)));

                if (result != null)
                {
                    await Navigation.PushAsync(new RoutePage(result));
                    //await DisplayAlert(result.Name, "Latitude: " + result.Coordinates.Latitude + "\nLongitude: " + result.Coordinates.Longitude, "OK");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.ToString(), "Oops");
            }

            //Draw line
            /*
            RequestFormLayout.IsVisible = false;

              var pin = new Pin
              {
                  Type = PinType.Place,
                  Position = new Position(18.4800103, -70.017052),
                  Label = "Xamarin San Francisco Office",
                  Address = "394 Pacific Ave, San Francisco CA",
                  Id = "Xamarin"
              };

              customMap.Pins.Add(pin);
              customMap.RouteCoordinates = new List<Position>(){
                    new Position(18.4800103, -70.017052),
                    new Position(18.4860341, -69.9277881),
              };
             
              customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(18.4860341, -69.9277881), Distance.FromMiles(3.0)));
*/
        }
    }
}