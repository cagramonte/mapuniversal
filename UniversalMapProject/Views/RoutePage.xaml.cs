﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.CrossPlacePicker.Abstractions;
using Refit;
using UniversalMapProject.Models;
using UniversalMapProject.Services;
using UniversalMapProject.Utils;
using UniversalMapProject.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace UniversalMapProject.Views
{
    public partial class RoutePage : ContentPage
    {
        int cIndex = 0;
        Places currentPlace;
        public RoutePage(Places places)
        {
            currentPlace = places;
            InitializeComponent();
            BindingContext = new RouteViewModel();

            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(18.4691796, -69.9403312), Xamarin.Forms.Maps.Distance.FromMiles(1.25f)));
            customMap.HasZoomEnabled = true;
            NavigationPage.SetBackButtonTitle(this, "Cancelar");
            GetDirections();
        }
        async Task GetDirections()
        {
            var googleMapsApi = RestService.For<IGoogleMapsApi>("https://maps.googleapis.com/maps");

            var response = await googleMapsApi.GetDirections($"18.4691796,-69.9403312",currentPlace.Name, "AIzaSyCmqzGFSQugS8bv5DgqgIfisdqh57wpBHI");

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var json = await response.Content.ReadAsStringAsync();
                var gModel = JsonConvert.DeserializeObject<GoogleDirection>(json);

                if(gModel.Routes.Count ==0)
                {
                    await DisplayAlert("Error", "No hay rutas disponibles para este destino", "OK");
                    await Navigation.PopToRootAsync();
                    return;
                }
                var list = Enumerable.ToList(PolylineUtils.Decode(gModel.Routes[0].OverviewPolyline.Points));
                customMap.RouteCoordinates = list;

              
                var lastPosition = list.LastOrDefault();
                var pin = new Pin
                {
                    Type = PinType.Place,
                    Position = new Position(lastPosition.Latitude,lastPosition.Longitude),
                    Label = currentPlace.Name,
                    Address = currentPlace.Address,
                    Id = "Destination"
                };
                customMap.Pins.Add(pin);
                var pin1 = new Pin
                {
                    Type = PinType.Generic,
                    Position = new Position(customMap.RouteCoordinates[0].Latitude, customMap.RouteCoordinates[0].Longitude),
                    Label = "Juan Perez",
                    Id = "Truck"
                };
                customMap.Pins.Add(pin1);
                cIndex = 0;

                customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(customMap.RouteCoordinates[0].Latitude, customMap.RouteCoordinates[0].Longitude), Xamarin.Forms.Maps.Distance.FromMiles(1.25f)));

                Device.StartTimer(TimeSpan.FromSeconds(5), () =>
                {
                    if (customMap.Pins.Count == 1)
                        return false;


                    
                    var cPin = customMap.Pins.LastOrDefault();
                    var retVal = true;
                    if(cPin!=null)
                    {
                        customMap.Pins.Remove(cPin);
                        cIndex++;
                        if (cIndex < customMap.RouteCoordinates.Count)
                        {
                          
                            customMap.Pins.Add(new Pin()
                            {
                                Type = PinType.Generic,
                                Position = new Position(customMap.RouteCoordinates[cIndex].Latitude, customMap.RouteCoordinates[cIndex].Longitude),
                                Label = "John Mendez",
                               Id = "Truck"
                            });
                         
                            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(customMap.RouteCoordinates[cIndex].Latitude, customMap.RouteCoordinates[cIndex].Longitude),customMap.VisibleRegion.Radius));

                        }
                        else
                        {
                            retVal = false;
                        }
                    }else
                    {
                        retVal = false;
                    }
                   
                    if(!retVal)
                    {
                        customMap.Pins.Clear();
                        customMap.RouteCoordinates.Clear();
                        End();
                    }
                    return retVal;

                });


      
            }
        }

        async Task End()
        {
            await DisplayAlert("Servicio Completado","Su servicio ha sido completado", "OK");
            await Navigation.PopToRootAsync();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
    }
}
